#include <iostream>
#include <vector>
using namespace std;

class Obj {
public:
    unsigned int id;
    unsigned int type;
    unsigned int weight;
};

class Que {
public:
    unsigned int id;
    unsigned int pri;
    unsigned int len;
    vector<int> objTypes;
    vector<Obj> objs;

    bool isTypeExit(unsigned int ObjType){
        for (int i = 0; i < objTypes.size(); ++i) {
            if(objTypes[i]==ObjType)
                return true;
        }
        return false;
    }

    bool isObjExit(unsigned int ObjID){
        for (int i = 0; i < objs.size(); ++i) {
            if(objs[i].id==ObjID)
                return true;
        }
        return false;
    }

    bool addObj(Obj obj){
        if(objs.size()==len){
            int index = -1;
            Obj minObj = findLowestObj(index);
            if(obj.weight>minObj){
                vector<Obj>::iterator it = objs.begin()+index;
                objs.erase(it);
                objs.push_back(obj);
            }else{
                return false;
            }
        }else{
            objs.push_back(obj);
        }

        return true;
    }

    Obj findLowestObj(int& index){
        int minWeight=21;
        for (int i = 0; i < objs.size(); ++i) {
           if( objs[i].weight <= minWeight ){
               minWeight = objs[i].weight;
               index = i;
           }
        }
        return objs[index];
    }
};

vector<Que> Ques;

bool isQueExit(unsigned int QueID);
Que findQue(unsigned int QueID);
bool isObjTypeSetted(unsigned int ObjType);

int main() {
    cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
    return 0;
}

int CreateQue(unsigned int QueID, unsigned int QuePri, unsigned int QueLen){
    if(QueID<1 || QueID>5)	return -1;
    if(QuePri<1 || QuePri>5) return -1;
    if(QueLen<1 || QueLen>0xFFFFFFFF) return -1;
    if(isQueExit(QueID)) return -1;

    Que que;
    que.id = QueID;
    que.pri = QuePri;
    que.len = QueLen;

    Ques.push_back(que);
    return 0;
}

bool isQueExit(unsigned int QueID){
    for (int i = 0; i < Ques.size(); ++i) {
        if(Ques[i].id == QueID)
            return true;
    }
    return false;
}

Que findQue(unsigned int QueID){
    for (int i = 0; i < Ques.size(); ++i) {
        if (Ques[i].id == QueID)
            return Ques[i];
    }
}

int SetObjTypeToQue(unsigned int ObjType, unsigned int QueID){
    if(ObjType<1 || ObjType>20)	return -1;
    if(QueID<1 || QueID>5)	return -1;
    if(!isQueExit(QueID)) return -1;

    if (isObjTypeSetted(ObjType)) return -1;

    Que que = findQue(QueID);
    que.objTypes.push_back(ObjType);
    return 0;
}

bool isObjTypeSetted(unsigned int ObjType){
    for (int i = 0; i < Ques.size(); ++i) {
        if (Ques[i].isTypeExit(ObjType))
            return true;
    }
    return false;
}

Que findQueByType(unsigned int ObjType){
    for (int i = 0; i < Ques.size(); ++i) {
        if (Ques[i].isTypeExit(ObjType))
            return Ques[i];
    }
}

int PushObj(unsigned int ObjID, unsigned int ObjType, unsigned int ObjWeight){
    if(ObjID<1 || ObjID>0xFFFFFFFF) return -1;
    if(ObjWeight<1 || ObjWeight>0xFFFFFFFF) return -1;
    if(ObjType<1 || ObjType>20)	return -1;
    if(!isObjTypeSetted(ObjType)) return -1;

    Obj obj;
    obj.id = ObjID;
    obj.type = ObjType;
    obj.weight = ObjWeight;

    Que que = findQueByType(ObjType);
    if (que.isObjExit(ObjID)) return -1;

    if(!que.addObj(obj)) return -1;

    return 0;
}

int PopObj(unsigned int* ObjID){

}

